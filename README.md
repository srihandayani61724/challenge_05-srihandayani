# Car Management Dashboard
pada challenge ini kita membuat manajemen data mobil ,diantaranya :

1.membuat http sever 
2.manajemen data mobil :- menambahkan data mobil 
                        - menghapus data mobil yang sudah ada , melihat daftar mobil yang tersedia di dalam database
3.membuat ERD dengan mengunakan dbdiagram.io                       


## How to run
guide ...
yarn init -y
touch index.js
yarn start


## Endpoints
list endpoints ...

## Directory Structure

```
.
├── config
│   └── config.json
├── controllers
├── migrations
├── models
│   └── index.js
├── public
│   ├── css
│   ├── fonts
│   ├── img
│   ├── js
│   └── uploads
├── seeders
├── views
│   ├── templates
│   │   ├── footer.ejs
│   │   └── header.ejs
│   ├── form.ejs
│   └── list.ejs
├── .gitignore
├── README.md
├── index.js
├── package.json
└── yarn.lock
```

## ERD
![Entity Relationship Diagram](public/img/erd.png)
