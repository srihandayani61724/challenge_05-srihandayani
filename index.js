const express = require('express');
const path = require('path')

const app = express()

// PORT with the default 3000
const PORT = process.env.PORT || 3000;

// Path in PUBLIC Directory
app.use(express.static(path.join(__dirname, 'public')));

// Set View Engine
app.set('view engine', 'ejs');

// GET Index
app.get('/', (req, res) => {
  res.render('index');
});

// Listening to PORT
app.listen(PORT, () => {
    console.log(`Server nyala di http://localhost:${PORT}`);
  });